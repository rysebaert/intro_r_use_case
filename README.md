# Vers une chaine de traitement reproductible


- [Voir le diaporama](https://rysebaert.gitpages.huma-num.fr/intro_r_use_case/#/)
- [Télécharger les données et les scripts](https://gitlab.huma-num.fr/rysebaert/intro_r_use_case/-/tree/master/download)
- [Télécharger le rapport de synthèse des analyses](https://gitlab.huma-num.fr/rysebaert/intro_r_use_case/-/blob/master/download/report.html)